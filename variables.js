// log to console
console.log('hello');
console.log(123);
console.log([1,2,3]);
console.log({a:1,b:2});
console.table({a:1,b:2});
console.error("this is error");
console.clear();
console.warn("this is warning");

console.time("hello");
    console.log('hello world');
    console.log('hello world');
    console.log('hello world');
    console.log('hello world');
    console.log('hello world');
    console.log('hello world');
    console.log('hello world');
console.timeEnd("hello");

// var, let, const

var name = "uzair";
console.log(name);
name = "ali";
console.log(name);

// Init, var

var greeting;
console.log(greeting);
greeting = "hello";
console.log(greeting);

// letters, numbers, _, $
// cannot start with number

// Multiword vars

var firstName = "uzair"; // camel case
var first_name = "sara"; // underscore case
var FirstName = "uzair" // pascal case


// LET

let name = "uzair";
console.log(name);
name = "ali";
console.log(name);

// CONST

const firstName = "john";
console.log(firstName);

// firstName = "uzair"; // cannot reassign

// const hello; // have to assign a value

const person = {
    name: "uzair",
    age: 30
}

person.name = "sara";
person.age = 56;

console.log(person);

const numbers = [1,2,3,4];
numbers.push(6);

console.log(numbers);


