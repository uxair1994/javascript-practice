// FUNCTION DECLARATIONS

function greet(name){
//  console.log("hello");
    if (typeof name === 'undefined') {
        name = 'uzair';
    }
    return `hello ${name}` ;
}

console.log(greet());


// FUNCTION EXRESSIONS

const square = function(x = 3){
    return x*x;
}
console.log(square());

// IMMEDIATELY INVOKABLE FUNCTION EXPRESSIONS IIFEs

(function(name){
    console.log(`hello ${name}`);
})('brad');


// PROPERTY METHODS

const todo = {
    add: function(){
        console.log('add todo');
    },
    edit: function(id){
        console.log(`edit this ${id}`);
    } 
}

todo.add();
todo.edit(15);