// PRIMITIVE

// String

const name = "uzair";
console.log(typeof name);

// Number

const age = 30;
console.log(typeof age);

// Boolean

const hasKids = true;
console.log(typeof hasKids);

// Null

const car = null;
console.log(typeof car);

// Undefined

let test;
console.log(typeof test);

// Symbol

const sym = Symbol();




// REFERENCES TYPES - OBJECTS

// Array

const hobbies = ['play','cricket'];
console.log(typeof hobbies);

// Objects literals

const address = {
    city: 'boston',
    state: 'usa'
}
console.log(typeof address);