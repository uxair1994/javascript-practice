const firstName = "uzair";
const lastName = "shamim";
const age = 26;
const str = "hello there is any other";
const tags = "webdesigner,web developer, ui developer";


let val;

// concatenation
val = firstName + ' ' + lastName;

// Append
val = "uzair ";
val += "shamim";

val = "hello my name is " + firstName + " and i am " + age + " years old";

// Escaping
val = 'that\ s awesome i cant\ t wait '

// Length
val = firstName.length;

// Concat
val = firstName.concat(' ',lastName);

// Change case
val = firstName.toUpperCase();
val = firstName.toLowerCase();

val = firstName[2];

// indexOf()
val = firstName.indexOf('r');
val = firstName.lastIndexOf('u');

// charAt()
val = firstName.charAt(2);

// Get last char
val = firstName.charAt(firstName.length -1);

// substring
val = firstName.substring(0,4);

// slice
val = firstName.slice(0,4);
val = firstName.slice(-3);

val = str.split(' ');
val = firstName.replace('uzair','ali');
val = tags.split(',');

console.log(val);