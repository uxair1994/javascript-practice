let val;

// Number to string
val = String(5);

// Boolean to string
val = String(true);

// Date to string
val = String(new Date());

// Array To String
val = String([1,2,3]);

// toString()
val = (856).toString();

// String to Number
val = Number('5');
val = Number(true);
val = Number(false);
val = Number(null);
val = Number('hello');
val = Number([1,2,3]);

val = parseInt('5632');
val = parseFloat('5632.30');

// Output
console.log(val);
console.log(typeof val);
console.log(val.length);