const num1 = 50;
const num2 = 60;


// Simple math with numbers
let add = num1 + num2;
console.log(add);

let sub = num1 - num2;
console.log(sub);

let mul = num1 * num2;
console.log(mul);

let div = num1 / num2;
console.log(div);

let mod = num1 % num2;
console.log(mod);

// Math Objects

let val;
val = Math.PI;
val = Math.E;
val = Math.round(2.4);
val = Math.ceil(2.4);
val = Math.floor(2.8);
val = Math.sqrt(64);
val = Math.abs(-3);
val = Math.pow(8,2);
val = Math.min(2,85,6,41,-3,-2);
val = Math.max(2,85,6,41,-3,-2);
val = Math.random();

val = Math.floor(Math.random() * 20);

console.log(val);
