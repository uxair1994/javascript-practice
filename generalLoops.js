// For Loop

for (let i = 0; i <= 10; i++) {
    
    if (i == 2) {
        
        console.log("2 is my favourite number");
        continue;
    }

    if (i == 5) {
        
        console.log("5 is my favourite number");
        break;
    }

    console.log('Number ' + i);

}

// While Loop

let i = 0;

while (i <= 10) {
    
    console.log("Number " + i);
    i++;

}


// DO WHILE

let i = 0;

do {
    console.log("Number " + i);
    i++;
} 
while (i < 10);


// Loop Through Array

const cars = ['audi','toyota','mehran','ferrari'];

// for (let index = 0; index < cars.length; index++) {
    
//     console.log(cars[index]);
    
// }

cars.forEach(function(car,index){
    console.log(`${index} : ${car}`);
})


// MAP

const users = [
    {id: 0, name: "uzair"},
    {id: 1, name: "ali"},
    {id: 2, name: "ahmed"},
]

const ids = users.map(function(user){
    return user.id;
})

console.log(ids);


const users = {
    firstName: 'john',
    age: 30
}

for (let x in users){
    console.log(`${x} : ${users[x]}`);
}