const id = 100;

// EQUAL TO

if (id == 100) {
    console.log("correct");
} else {
    console.log("incorrect");
}

// NOT EQUAL TO

if (id != 101) {
    console.log("correct")
} else {
    console.log("incorrect");
}

// EQUAL TO VALUE AND TYPE

if (id === 100) {
    console.log("correct");
} else {
    console.log("incorrect");
}

if (typeof id !== 'undefined') {
    console.log(`there is no id is defined`);
} else {
    console.log("no id");
}

// IF - ELSE

const color = "yellow";

if (color === 'red') {
    console.log("color is red");
}
else if(color === 'blue'){
    console.log("color is blue");
}
else if(color === 'black'){
    console.log("color is black");
}
else{
    console.log("color are others");
}

// LOGICAL OPERATORS

const name = "steve";
age = 26;

if (age > 0 && age < 12) {
    console.log(`${name} is child`);
} else if(age >= 13 && age <= 17){
    console.log(`${name} is teenage`);
}else{
    console.log(`${name} is adult`);
}


// TERNARY OPERATOR

console.log(id === 100 ? 'correct' : 'not correct');