const name = "uzair shamim";
const age = 26;
const job = "web developer";
const city = "karachi";

let html;

// without template strings (es5)

html = '<ul><li>Name : ' + name + '</li><li>Age : ' + age + '</li><li>Job : ' + job + '</li><li>City : ' + city + '</li></ul>'

html =  '<ul>'+
            '<li>Name : '+name+'</li>'+
            '<li>Age : '+age+'</li>'+
            '<li>job : '+job+'</li>'+
            '<li>city : '+city+'</li>'+
        '</ul>'

// template literals

function hello(){
    return 'hello';
}

html = `
    <ul>
        <li>Name : ${name}</li>
        <li>Age : ${age}</li>
        <li>Job : ${job}</li>
        <li>City : ${city}</li>
        <li>${hello()}</li>
    </ul>
`

document.body.innerHTML = html;