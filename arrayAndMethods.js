const numbers = [23,56,74,2,68,71];
const numbers2 = new Array(22,45,85,77,14,65);
const fruit = ['apple','banana','grapes','olive'];
const mixed = ['22',true,undefined,null, {a:1,b:1}, new Date()];

let val;

// Get array length
val = numbers.length;

// check if is array
val = Array.isArray(numbers);

// Get single value
val = numbers[0];
val = numbers[2];

// Insert into array
numbers[2] = 100;

// find indexOf value
val = numbers.indexOf(71);

// Mutating Arrays
// Add to end
numbers.push(250);

// Add on to front
numbers.unshift(30);

// Take off from front
numbers.shift();

// Take off from end
numbers.pop();

// Splice values
numbers.splice(1,2);

// Numbers reverse
numbers.reverse();

// Concatenate array
val = numbers.concat(numbers2);

// val = fruit.sort();
val = numbers.sort();


console.log(numbers);
console.log(val);